package utils

import (
	"encoding/json"
	"net/http"
)

const (
	UrlLocalhost    = "http://localhost"
	ContentType     = "Content-Type"
	ContentTypeJson = "application/json"
)

type ErrorResponse struct {
	Message string `json:"message"`
}

func WriteErrorResponse(w http.ResponseWriter, httpCode int, message string) {
	w.Header().Set(ContentType, ContentTypeJson)
	w.WriteHeader(httpCode)
	if message != "" {
		resp := ErrorResponse{Message: message}
		respJson, _ := json.Marshal(resp)
		_, _ = w.Write(respJson)
	}
}

func WriteOkJsonResponse(w http.ResponseWriter, response []byte) {
	w.Header().Set(ContentType, ContentTypeJson)
	w.WriteHeader(http.StatusOK)
	if response != nil {
		_, _ = w.Write(response)
	}
}
