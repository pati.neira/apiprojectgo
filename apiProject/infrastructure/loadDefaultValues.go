package infrastructure

import (
	"embed"
	"encoding/json"
	"log"
)

var (
	//go:embed loads/users/*
	userFiles embed.FS
)

const (
	DB_NAME          = "local"
	COLLECTION_USERS = "users"
	dirUsers         = "loads/users"
)

func (it *DbHandler) loadDefaultValues() {
	it.loadUsers()
}

func (it *DbHandler) loadUsers() {
	files, err := userFiles.ReadDir(dirUsers)
	if err != nil {
		log.Println(err)
		log.Fatal("Error when get users directory files")
		return
	}
	coll, ctx := it.GetDbCollection(DB_NAME, COLLECTION_USERS)
	err = coll.Drop(ctx)
	if err != nil {
		log.Println(err)
		log.Fatal("Can't drop collection" + COLLECTION_USERS)
		return
	}

	for _, file := range files {
		data, err := userFiles.ReadFile(dirUsers + "/" + file.Name())
		if err != nil {
			log.Println(err)
			log.Fatal("Error when read file")
			return
		}
		var dataInterface []interface{}
		err = json.Unmarshal(data, &dataInterface)
		if err != nil {
			log.Println(err)
			log.Fatal("Error when marshal file")
			return
		}
		_, err = coll.InsertMany(ctx, dataInterface)
		if err != nil {
			log.Println(err)
			log.Fatal("Error when marshal file")
			return
		}
	}
}
