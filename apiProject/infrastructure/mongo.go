package infrastructure

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"sync"
	"time"
)

const (
	MONGO_URL = "mongodb://localhost:27017"
)

type DbHandler struct {
	database string
	client   *mongo.Client
}

var (
	singleton *DbHandler
	once      sync.Once
)

func GetMongoDbHandler() *DbHandler {
	if singleton == nil {
		once.Do(func() {
			singleton = &DbHandler{}
			singleton.initMongo()
		})
	}
	return singleton
}

func (it *DbHandler) initMongo() {
	var err error
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	it.client, err = mongo.Connect(ctx, options.Client().ApplyURI(MONGO_URL))
	if err != nil {
		log.Fatal(err)
	}
	err = it.client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	it.loadDefaultValues()
}

func (it *DbHandler) GetDbCollection(dbName string, collectionName string) (*mongo.Collection, context.Context) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := it.client.Database(dbName).Collection(collectionName)
	return collection, ctx
}

func GetMongoDbHandlerForTesting() *DbHandler {
	if singleton == nil {
		once.Do(func() {
			singleton = &DbHandler{}
			singleton.initMongo()
		})
	}
	return singleton
}
