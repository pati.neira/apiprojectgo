package main

import (
	"apiProject/injections"
)

func main() {
	injections.SetupHttpServer()
}
