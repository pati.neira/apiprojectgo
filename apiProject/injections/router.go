package injections

import (
	"apiProject/infrastructure"
	"apiProject/users"
	"github.com/facebookgo/inject"
	"github.com/go-chi/chi/v5"
	"github.com/rs/cors"
	"log"
	"net/http"
	"sync"
)

type IRouter interface {
	setupRoutes() *chi.Mux
}

type router struct {
}

var (
	routerSingleton *router
	routerOnce      sync.Once
)

func SetupHttpServer() {
	corsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{http.MethodGet, http.MethodPut},
		AllowedHeaders: []string{"*"},
	}).Handler(newRouter().setupRoutes())

	err := http.ListenAndServe(":8090", corsHandler)

	if err == nil {
		log.Println("Server shut down because of error: ", err)
	}
}

func newRouter() IRouter {
	if routerSingleton == nil {
		routerOnce.Do(func() {
			routerSingleton = &router{}
		})
	}
	return routerSingleton
}

func (router *router) setupRoutes() *chi.Mux {
	r := chi.NewRouter()

	var controller users.Controller

	var graph inject.Graph
	err := graph.Provide(
		&inject.Object{Value: &controller},
		&inject.Object{Value: infrastructure.GetMongoDbHandler()},
		&inject.Object{Value: &users.UseCaseGetUsersMultipleFilter{}},
		&inject.Object{Value: &users.UseCaseUpdateUsersAttributes{}},
		&inject.Object{Value: &users.UseCaseGetDistinctAttributes{}},
		&inject.Object{Value: &users.Repository{}},
	)
	if err != nil {
		log.Fatal(err)
	}
	err = graph.Populate()
	if err != nil {
		log.Fatal(err)
	}

	r.Group(func(r chi.Router) {
		r.Get(controller.EndpointGetUser(), controller.GetUser)
		r.Put(controller.EndpointPutUserAttributes(), controller.PutAttributes)
		r.Get(controller.EndpointGetDistinctAttributes(), controller.GetDistincAttributes)
	})

	return r
}
