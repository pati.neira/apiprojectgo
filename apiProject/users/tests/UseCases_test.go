package tests

import (
	"apiProject/users"
	"apiProject/users/tests/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUseCaseGetUsersMultipleFilter_NoInfo(t *testing.T) {
	ctrl := gomock.NewController(t)
	// Assert that Bar() is invoked.
	defer ctrl.Finish()

	repository := mocks.NewMockIRepository(ctrl)

	expectedError := users.ErrorNotRecords

	repository.EXPECT().GetUsersMultipleFilters(gomock.Any()).
		Return(nil, expectedError).
		Times(1)

	var filtersParam map[string]interface{}
	filtersParam["key1"] = true
	filtersParam["key2"] = false

	useCase := users.UseCaseGetUsersMultipleFilter{Repository: repository}

	users, err := useCase.GetUsersMultipleFilter(filtersParam)

	assert.Equal(t, expectedError, err)
	assert.Nil(t, users)
}
