package users

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (it *Controller) mapUserRequestToDomain(user UserRequest) User {
	return User{
		Id:         user.Id,
		Attributes: user.Attributes,
	}
}

func (it *Controller) mapUserDomainToResponse(user User) UserResponse {
	return UserResponse{
		Id:         user.Id,
		Name:       user.Name,
		Age:        user.Age,
		Attributes: user.Attributes,
	}
}

func (it *Repository) mapUserDomainToDB(user User) UserDB {
	idMongo, _ := primitive.ObjectIDFromHex(user.Id)
	return UserDB{
		Id:         idMongo,
		Name:       user.Name,
		Age:        user.Age,
		Attributes: user.Attributes,
	}
}

func (it *Repository) mapUserDBtoDomain(user UserDB) User {
	id := user.Id.Hex()
	return User{
		Id:         id,
		Name:       user.Name,
		Age:        user.Age,
		Attributes: user.Attributes,
	}
}

func (it *Repository) mapUsersDBtoDomain(usersdb []UserDB) []User {
	users := make([]User, 0)
	for _, usrDb := range usersdb {
		users = append(users, it.mapUserDBtoDomain(usrDb))
	}
	return users
}

func (it *Controller) mapUsersDomainToResponse(users []User) []UserResponse {
	usersResp := make([]UserResponse, 0)
	for _, usr := range users {
		usersResp = append(usersResp, it.mapUserDomainToResponse(usr))
	}
	return usersResp
}

func (it *Controller) mapUsersRequestToDomain(usersReq []UserRequest) []User {
	users := make([]User, 0)
	for _, usr := range usersReq {
		users = append(users, it.mapUserRequestToDomain(usr))
	}
	return users
}
