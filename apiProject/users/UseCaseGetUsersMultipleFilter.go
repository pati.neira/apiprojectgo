package users

import (
	"fmt"
	"log"
	"strconv"
)

type IUseCaseGetUsersMultipleFilter interface {
	GetUsersMultipleFilter(filters map[string]interface{}) ([]User, error)
}

type UseCaseGetUsersMultipleFilter struct {
	Repository IRepository `inject:""`
}

func (it UseCaseGetUsersMultipleFilter) GetUsersMultipleFilter(params map[string]interface{}) ([]User, error) {
	filters := make(map[string]interface{})
	for key, value := range params {
		switch key {
		case COL_NAME:
			filters[key] = value
		case COL_AGE:
			valNumber, err := strconv.ParseInt(fmt.Sprintf("%v", value), 10, 32)
			if err != nil {
				log.Println(ErrorFormatNotCorrect)
				return nil, ErrorFormatNotCorrect
			}
			filters[key] = valNumber
		default:
			valBool, err := strconv.ParseBool(fmt.Sprintf("%v", value))
			if err != nil {
				log.Println(ErrorFormatNotCorrect)
				return nil, ErrorFormatNotCorrect
			}
			filters[COL_ATTRIBUTES_CONTENT+key] = valBool
		}
	}
	users, err := it.Repository.GetUsersMultipleFilters(filters)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	if len(users) == 0 {
		log.Println(ErrorNotRecords)
		return nil, ErrorNotRecords
	}

	return users, nil
}
