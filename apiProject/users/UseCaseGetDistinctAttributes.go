package users

import "log"

type IUseCaseGetDistinctAttributes interface {
	GetDistinctAttributes(id string) (interface{}, error)
}

type UseCaseGetDistinctAttributes struct {
	Repository IRepository `inject:""`
}

func (it UseCaseGetDistinctAttributes) GetDistinctAttributes(id string) (interface{}, error) {
	results, err := it.Repository.Distinct(id)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return results, err
}
