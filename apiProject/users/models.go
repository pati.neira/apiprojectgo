package users

import "go.mongodb.org/mongo-driver/bson/primitive"

type UserDB struct {
	Id         primitive.ObjectID `bson:"_id"`
	Name       string             `bson:"name"`
	Age        int                `bson:"age"`
	Attributes map[string]bool    `bson:"attributes"`
}

type User struct {
	Id         string
	Name       string
	Age        int
	Attributes map[string]bool
}

type UserResponse struct {
	Id         string          `json:"id"`
	Name       string          `json:"name"`
	Age        int             `json:"age"`
	Attributes map[string]bool `json:"attributes"`
}

type UserRequest struct {
	Id         string          `json:"id"`
	Attributes map[string]bool `json:"attributes"`
}

type AttributesResponse struct {
	Attributes interface{} `json:"attributes"`
}
