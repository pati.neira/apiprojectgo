package users

import (
	"apiProject/utils"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"io/ioutil"
	"log"
	"net/http"
)

type Controller struct {
	UseCaseGetUsersMultipleFilter IUseCaseGetUsersMultipleFilter `inject:""`
	UseCaseUpdateUsersAttributes  IUseCaseUpdateUsersAttributes  `inject:""`
	UseCaseGetDistinctAttributes  IUseCaseGetDistinctAttributes  `inject:""`
}

const (
	ID_URL_PARAM = "id"
)

func (it *Controller) EndpointGetUser() string {
	return "/user"
}

func (it *Controller) EndpointPutUserAttributes() string {
	return "/user/attributes"
}

func (it *Controller) EndpointGetDistinctAttributes() string {
	return "/user/{id}/attributes"
}

func (it *Controller) GetUser(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	filters := make(map[string]interface{})
	for key, _ := range params {
		filters[key] = r.URL.Query().Get(key)
	}
	log.Println(filters)
	users, err := it.UseCaseGetUsersMultipleFilter.GetUsersMultipleFilter(filters)
	if err != nil {
		switch err {
		case ErrorFormatNotCorrect:
			utils.WriteErrorResponse(w, http.StatusBadRequest, ErrorFormatNotCorrect.Error())
			return
		case ErrorNotRecords:
			utils.WriteErrorResponse(w, http.StatusNotFound, ErrorNotRecords.Error())
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	response, _ := json.Marshal(it.mapUsersDomainToResponse(users))
	utils.WriteOkJsonResponse(w, response)
	return
}

func (it *Controller) PutAttributes(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	usersReq := make([]UserRequest, 0)
	err = json.Unmarshal(body, &usersReq)
	if err != nil {
		log.Println(err)
		utils.WriteErrorResponse(w, http.StatusBadRequest, ErrorUnmarshal.Error())
		return
	}
	users := it.mapUsersRequestToDomain(usersReq)
	err = it.UseCaseUpdateUsersAttributes.UpdateUsersAttributes(users)
	if err != nil {
		switch err {
		case ErrorNotRecords:
			utils.WriteErrorResponse(w, http.StatusNotFound, ErrorNotRecords.Error())
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	return
}

func (it *Controller) GetDistincAttributes(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, ID_URL_PARAM)
	attr, err := it.UseCaseGetDistinctAttributes.GetDistinctAttributes(id)
	if err != nil {
		switch err {
		case ErrorNotRecords:
			utils.WriteErrorResponse(w, http.StatusNotFound, ErrorNotRecords.Error())
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	attrResp := AttributesResponse{Attributes: attr}
	response, _ := json.Marshal(attrResp)
	utils.WriteOkJsonResponse(w, response)
	return
}
