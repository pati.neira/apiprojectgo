package users

import "errors"

var ErrorFormatNotCorrect = errors.New("Parameter value not correct")
var ErrorParamNotCorrect = errors.New("Parameter key not correct")
var ErrorNotRecords = errors.New("Not found records")
var ErrorUnmarshal = errors.New("Error when json.unmarshal")
