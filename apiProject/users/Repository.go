package users

import (
	"apiProject/infrastructure"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
)

const (
	DB_NAME          = "local"
	COLLECTION_USERS = "users"
)

const (
	COL_ID                 = "_id"
	COL_NAME               = "name"
	COL_AGE                = "age"
	COL_ATTRIBUTES_CONTENT = "attributes."
	COL_ATTRIBUTES         = "attributes"
)

//go:generate mockgen -destination=tests/mocks/mockIRepository.go -package=mocks . IRepository
type IRepository interface {
	GetUsersMultipleFilters(filters map[string]interface{}) ([]User, error)
	UpdateUserAttributes(user User) error
	Distinct(id string) (interface{}, error)
}

type Repository struct {
	Mongo *infrastructure.DbHandler `inject:""`
}

func (it *Repository) GetUsersMultipleFilters(filtersParam map[string]interface{}) ([]User, error) {
	collection, ctx := it.Mongo.GetDbCollection(DB_NAME, COLLECTION_USERS)

	filterArray := []bson.M{}
	for key, value := range filtersParam {
		filterArray = append(filterArray, bson.M{key: value})
	}

	filter := bson.M{"$and": filterArray}

	users := make([]UserDB, 0)
	cursor, err := collection.Find(ctx, filter)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var entity UserDB
		err := cursor.Decode(&entity)
		if err != nil {
			log.Println(err)
			return nil, err
		}
		users = append(users, entity)
	}

	log.Println(users)
	return it.mapUsersDBtoDomain(users), nil
}

func (it *Repository) UpdateUserAttributes(user User) error {
	userDb := it.mapUserDomainToDB(user)
	filter := bson.M{COL_ID: userDb.Id}

	collection, ctx := it.Mongo.GetDbCollection(DB_NAME, COLLECTION_USERS)
	update := bson.D{{"$set",
		bson.D{
			{COL_ATTRIBUTES, userDb.Attributes},
		},
	}}

	result, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		log.Println(err)
		return err
	}
	if result.MatchedCount == 0 {
		log.Println(ErrorNotRecords)
		return ErrorNotRecords
	}

	return nil
}

func (it *Repository) Distinct(id string) (interface{}, error) {
	idMongo, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{COL_ID: idMongo}

	collection, ctx := it.Mongo.GetDbCollection(DB_NAME, COLLECTION_USERS)

	results, err := collection.Distinct(ctx, COL_ATTRIBUTES, filter)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	log.Println(results)

	return results, nil
}
