package users

import "log"

type IUseCaseUpdateUsersAttributes interface {
	UpdateUsersAttributes(users []User) error
}

type UseCaseUpdateUsersAttributes struct {
	Repository IRepository `inject:""`
}

func (it UseCaseUpdateUsersAttributes) UpdateUsersAttributes(users []User) error {
	for _, user := range users {
		err := it.Repository.UpdateUserAttributes(user)
		if err != nil {
			log.Println(err)
			return err
		}
	}
	return nil
}
