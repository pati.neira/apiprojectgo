# ApiProjectGo

## Getting started

To be able to run the application we need to run mongo using Docker. 
With these commands:

```
docker pull mongo
docker run --name api-db -p 27017:27017 -d mongo:latest
```

## Endpoints 
When the Api runs some default data is upload.

## Enpoint Get Distinct

Refers to instruction: "returns a distinct list of all known attributes"
(I was not sure about the correct behavior you spec but I did it from my undestanding)

You send the user id and returns the attributes.

Get -> http://localhost:8090/user/623d697b1e1b39bd8561c453/attributes

Result body:

```
{
    "attributes": [
        [
            {
                "Key": "happy",
                "Value": true
            },
            {
                "Key": "healthy",
                "Value": false
            },
            {
                "Key": "busy",
                "Value": true
            }
        ]
    ]
}
```

## Enpoint Get Users from params

Refers to this instruction: "returns list of users with assigned attributes"

Get -> http://localhost:8090/user?age=20&healthy=false

You may add as many parameters you want. The find in the DB will be filter from them.

The result is all the users that match.

```
[
    {
        "id": "623d5f3934c52064660354e7",
        "name": "Jason",
        "age": 20,
        "attributes": {
            "busy": true,
            "happy": true,
            "healthy": false
        }
    },
    {
        "id": "623d5f3923456yhjbgfty4567",
        "name": "Jack",
        "age": 21,
        "attributes": {
            "busy": true,
            "happy": true,
            "healthy": false
        }
    }
]
```

## Enpoint Add and Remove User's attributes

Refers to this instruction: "allow the addition and removal of columns from the table. only attributes are allowed to be columns"

Put -> http://localhost:8090/user/attributes

You may add or remove attributes from users. 
Depends on how you send the information.

- id = is the id user
- attributes = the attributes inside are the ones that will be save in the DB for the specific user

Example body to send it:

```
[
    {
        "id": "623d39c779c18a72af2c4e98",
        "attributes": {
            "happy": true,
            "healthy": false,
            "newAttribute":true
        }
    },
    {
        "id": "623d39c779c18a72af2c4e99",
        "attributes": {
            "busy": true,
            "healthy": false,
            "newAttribute":true
        }
    },
    {
        "id": "623d39c779c18a72af2c4e9a",
        "attributes": {
            "busy": true,
            "happy": true,
            "healthy": true,
            "newAttribute":false
        }
    }
]
```



## Comments

If something is wrong or some information is not allowed or found.
The api send a specific statusCode (httpCode) and sometimes a message.

I tried to implement dockerfile but I couldn't finish.
